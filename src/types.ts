import Vue from 'vue';

export interface LayoutProps {}

declare module 'vue/types/vue' {
  interface Vue {
    layoutProps: LayoutProps;
  }
}

declare module 'vue/types/options' {
  interface ComponentOptions<V extends Vue> {
    layoutProps?(): LayoutProps;
  }
}
