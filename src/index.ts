import { Component, Vue } from 'nuxt-property-decorator';
import { Route } from 'vue-router';
import { LayoutProps } from './types';
import { NoCache } from './utils';

@Component
class LayoutPropsMixin extends Vue {
  /** Identifier timestamp */
  public timestamp = 0;

  /** ストアの購読をやめるための関数 */
  private unsubscribeStore?: Function;

  /** ナビゲーションの購読をやめるための関数 */
  private unsubscribeNavigation?: Function;

  /**
   * @constructor
   */
  public constructor() {
    super();

    this.onNavigation = this.onNavigation.bind(this);
    this.updateTimestamp = this.updateTimestamp.bind(this);
  }

  /** Lifecycle event */
  public mounted(): void {
    this.updateTimestamp();

    if (this.$store) {
      this.unsubscribeStore = this.$store.subscribe(this.updateTimestamp);
    }
    this.unsubscribeNavigation = this.$router.afterEach(this.onNavigation);
  }

  /** Lifesycle event */
  public beforeDestroy(): void {
    if (this.unsubscribeStore) {
      this.unsubscribeStore();
    }

    if (this.unsubscribeNavigation) {
      this.unsubscribeNavigation();
    }
  }

  /** Layout Props */
  public get layoutProps(): LayoutProps {
    // @ts-ignore
    const { pageComponent, $nuxt } = this;

    if (!this.timestamp) return {};

    if (!pageComponent) return {};

    const { $options } = pageComponent;
    const { layoutProps } = $options;

    if (!layoutProps) return {};

    const props = layoutProps.call(pageComponent);
    const isError = !!($nuxt as any).nuxt.err;

    return {
      ...props,
      isError,
    };
  }

  /** Reference page component */
  @NoCache
  public get pageComponent(): Vue | void {
    const { componentInstance } = this.$vnode;

    if (!componentInstance) return;

    const nuxt = componentInstance.$children.find((component: any) => (
      typeof component.nuxt === 'object' && component._name === '<Nuxt>'
    ));

    if (!nuxt) return;

    return nuxt.$children[0];
  }

  /**
   * Update identifier timestamp
   */
  private updateTimestamp(): void {
    this.timestamp = Date.now();
  }

  /**
   * on navigation
   */
  private onNavigation(to: Route): void {
    const matchedComponent = to.matched[0];

    if (!matchedComponent) return;

    const pageComponent: any = matchedComponent.components.default;
    const { options } = pageComponent;

    const updateTimestamp = (() => {
      this.updateTimestamp();

      options.created = options.created.filter((cb: Function) => cb !== updateTimestamp);
    });

    options.created.push(updateTimestamp);
  }
}

export default LayoutPropsMixin;
