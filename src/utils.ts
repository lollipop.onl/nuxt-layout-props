import { createDecorator } from 'vue-class-component';

/**
 * No cache Computed.get
 */
export const NoCache = createDecorator((options, key) => {
  // @ts-ignore
  options.computed[key].cache = false;
});
